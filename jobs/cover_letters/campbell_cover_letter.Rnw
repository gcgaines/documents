% source(file.path("C:/gaines/projects/dissertation/functions/knit2sumatra.R"))
% setwd("C:/gaines/documents/jobs/cover_letters")
% knit2sumatra('campbell_cover_letter')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%------RMIT Letterhead------------%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,a4paper]{letter}

% you may specify the font size (10pt, 11pt and 12pt) and paper size (letterpaper, a4paper, etc.)
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{gfsdidot}
\usepackage{pxfonts}
\usepackage[T1]{fontenc}

% Create a new command for the horizontal rule in the document which allows thickness specification
\makeatletter
\def\vhrulefill#1{\leavevmode\leaders\hrule\@height#1\hfill \kern\z@}
\makeatother

%----------------------------------------------------------------------------------------
%	PAPER MARGINS
%----------------------------------------------------------------------------------------

\textwidth 6.75in
\textheight 9.25in
\oddsidemargin -.25in
\evensidemargin -.25in
\topmargin -1in
\longindentation 0.50\textwidth
\parindent 0.4in

%----------------------------------------------------------------------------------------
%	SENDER INFORMATION
%----------------------------------------------------------------------------------------

\def\Who{George C. Gaines III}
\def\What{Ph.D. Candidate, Forest Biometrics}
\def\Where{Department of Forest Management\\W.A. Franke College of \\Forestry and Conservation}
\def\Address{CHCB 403 $\cdot$ 32 Campus Dr}
\def\CityZip{Missoula, MT USA 59812}
\def\citizen{US Citizen $\cdot$ Non-veteran}
\def\Email{E-mail: georgecgaines@gmail.com}
\def\TEL{Phone: +1 406 880 3050}
\def\URL{}
%----------------------------------------------------------------------------------------
%	HEADER AND FROM ADDRESS STRUCTURE
%----------------------------------------------------------------------------------------
%%%%

\address{
\includegraphics[width=2.5in]{um_logo_transparent.png} % Include the logo of your institution
\hspace{4.0in} % Position of the institution logo, increase to move left, decrease to move right
\vskip -1.1in~\\ % Position of the text in relation to the institution logo, increase to move down, decrease to move up
%\makebox[0ex][r]{\bf \Who \What }\hspace{0.08in}
~\\[-0.11in] % Reduce the whitespace above the horizontal rule
\hspace{\fill}\parbox[t]{2.85in}{ % Create a box for your details underneath the horizontal rule on the right
\footnotesize % Use a smaller font size for the details
\Who \\ \em % Your name, all text after this will be italicized
\What\\
\Where\\ % Your department
\Address\\ % Your address
\CityZip\\ % Your city and zip code
\citizen\\
\TEL\\ % Your phone number
\Email\\ % Your email address
\URL % Your URL
}
\hspace{-1.3in} % Horizontal position of this block, increase to move left, decrease to move right
\vspace{-1in} % Move the letter content up for a more compact look
}

%----------------------------------------------------------------------------------------
%	TO ADDRESS STRUCTURE
%----------------------------------------------------------------------------------------
\def\opening#1{\thispagestyle{empty}
%%% from address
{\centering\fromaddress \vspace{1.1in} \\
%%% Date
\today \hspace*{\fill}\par} % remove \today to not display it
%%% Title of letter
{\raggedright \textbf{\large{}}}\\
{\raggedright \toname \\ \toaddress \par} % Print the to name and address
%\vspace{0.15in} % White space after the to address
\noindent #1 % Print the opening line
}

%----------------------------------------------------------------------------------------
%	SIGNATURE
%----------------------------------------------------------------------------------------

\signature{\Who \What}

\long\def\closing#1{
\vspace{0.2in} % Some whitespace after the letter content and before the signature
\noindent % Stop paragraph indentation
\hspace*{0.9cm} % Move the signature right
\parbox{\indentedwidth}{\raggedright
#1 % Print the signature text
\vskip 0.15in % Whitespace between the signature text and your name
%\fromsig}}
}}
%----------------------------------------------------------------------------------------

\begin{document}

%----------------------------------------------------------------------------------------
%	TO ADDRESS
%----------------------------------------------------------------------------------------

\begin{letter}
{Application for Biometrician Position\\
Campbell Global LLC \\
1300 SW 5th Ave, Suite 3200\\
Portland, Oregon 97201
%\vspace{0.05in}
}

%----------------------------------------------------------------------------------------
%	LETTER CONTENT
%----------------------------------------------------------------------------------------

\opening{To the hiring committee,}
\vskip 0.05in
My name is George Gaines and I am a Ph.D. candidate in Forestry 
and Conservation Sciences in the Department of Forest Management, 
W.A. Franke College of Forestry and Conservation, at the University 
of Montana in Missoula, MT USA. My faculty advisor is Dr. David Affleck, 
Professor of Forest Biometrics \& Chair, Department of Forest Management. 
I have completed the Ph.D. coursework requirements, passed my comprehensive 
examinations, and my dissertation research is nearing completion. My 
dissertation defense is scheduled for December 13, 2021. I am very excited 
to apply for this forest biometrician position with Campbell. 

My love for forests stems from childhood summers spent among 
the coast redwoods of Mendocino County, California. During 10 
years in Montana and a year working on the Weyerhaeuser WA
Forest Resources Team, that love grew to encompass diverse landscapes 
spanning the full spectrum of western ecotones. In my time as a Ph.D. 
research and teaching assistant at UM, I evolved from a curious 
student of the quantitative properties of natural systems into a versatile 
statistician, forest analyst, and research scientist.

A graduate coursework plan oriented on a theoretical statistical 
track prepared me for a career at the intersection of forest inventory, 
sampling methods, growth \& yield modeling, remote sensing, 
big data analysis, and financial analysis. Further, my Bachelors of Science in 
Forest Resource Management from UM, coupled with graduate coursework 
in remote sensing analysis in Google Earth Engine; lidar and vegetation 
analysis; forest growth modeling; landscape ecology; and seminars in 
systems ecology solidified my understanding of the application of new 
technologies, statistical methods and programming techniques to critical 
environmental analyses.

Through my dissertation research, collectively titled, 
``Small area estimation of post-fire tree regeneration in the western 
US using an annualized nationwide forest inventory'', I was afforded 
the invaluable opportunity to put all the pieces together. Wrangling 
nearly 50,000 FIA subplot observations of post-fire vegetation attributes 
taught me to navigate the FIA sample design and tabular database structure. 
Compilation of wide-ranging climatic, biophysical, and other datasets 
in cloud-based remote sensing analysis platform Google Earth Engine 
and importation to \texttt{R} for analysis taught me to apply 
cutting-edge remote sensing technologies and program complex modeling 
routines to address pressing forest research questions using large datasets. 
The implementation and comparison of competing small area estimators of 
post-fire tree cover enabled me to further fuse the realms of traditional 
quantitative methods, emerging estimation strategies, machine learning, 
fire ecology, and forest science. This work and educational experience 
helped me develop expertise in the areas of biometrics, geospatial 
analyses, data visualization and analytics, field and office inventory 
techniques, and devising project objectives and research questions.

I believe I have a unique aptitude for operating across the disciplinary 
boundary between the mechanics of quantitative ecological analyses and 
the dynamic timberland management decision-making processes such analyses 
support, in settings both collaborative and independent alike. The prospect 
of making an impact as a member of a team optimizing Campbell Global 
timberland management practices is extremely exciting.

My contributions to diversity, equity and inclusion; my educational and 
experiential background; and my insatiable appetite for discovery and 
lifelong learning through scientific inquiry are outlined in my attached 
CV. I believe these traits, together with my deep desire to make a positive 
impact as a member of the Campbell Global team, suit me uniquely 
for this biometrician position. 

\closing{Sincerely,}
%----------------------------------------------------------------------------------------

\includegraphics[width=.3\textwidth]{transp_signature_1.png}

\textbf{George C. Gaines III}

\end{letter}

\end{document}

%% end of file `template.tex'.