% source(file.path("C:/gaines/projects/dissertation/functions/knit2sumatra.R"))
% setwd("C:/gaines/documents/jobs/research_teaching_statements")
% knit2sumatra('ubc_asst_prof_biometrics_ed_leadership_stmnt')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%------RMIT Letterhead------------%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,a4paper]{letter}

% you may specify the font size (10pt, 11pt and 12pt) and paper size (letterpaper, a4paper, etc.)
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{gfsdidot}
\usepackage{pxfonts}
\usepackage[T1]{fontenc}

% Create a new command for the horizontal rule in the document which allows thickness specification
\makeatletter
\def\vhrulefill#1{\leavevmode\leaders\hrule\@height#1\hfill \kern\z@}
\makeatother

%----------------------------------------------------------------------------------------
%	PAPER MARGINS
%----------------------------------------------------------------------------------------

\textwidth 6.75in
\textheight 9.25in
\oddsidemargin -.25in
\evensidemargin -.25in
\topmargin -1in
\longindentation 0.50\textwidth
\parindent 0.4in

%----------------------------------------------------------------------------------------
%	SENDER INFORMATION
%----------------------------------------------------------------------------------------

\def\Who{George C. Gaines III}
\def\What{PhD Candidate, Forest Biometrics}
\def\Where{Department of Forest Management\\W.A. Franke College of \\Forestry and Conservation}
\def\Address{CHCB 403 $\cdot$ 32 Campus Dr}
\def\CityZip{Missoula, MT USA 59812}
\def\citizen{US Citizen $\cdot$ Non-veteran}
\def\Email{E-mail: georgecgaines@gmail.com}
\def\TEL{Phone: +1 406 880 3050}
\def\URL{}
%----------------------------------------------------------------------------------------
%	HEADER AND FROM ADDRESS STRUCTURE
%----------------------------------------------------------------------------------------
%%%%

\address{
\includegraphics[width=2.5in]{um_logo_transparent.png} % Include the logo of your institution
\hspace{4.0in} % Position of the institution logo, increase to move left, decrease to move right
\vskip -1.1in~\\ % Position of the text in relation to the institution logo, increase to move down, decrease to move up
%\makebox[0ex][r]{\bf \Who \What }\hspace{0.08in}
~\\[-0.11in] % Reduce the whitespace above the horizontal rule
\hspace{\fill}\parbox[t]{2.85in}{ % Create a box for your details underneath the horizontal rule on the right
\footnotesize % Use a smaller font size for the details
\Who \\ \em % Your name, all text after this will be italicized
\What\\
\Where\\ % Your department
\Address\\ % Your address
\CityZip\\ % Your city and zip code
\citizen\\
\TEL\\ % Your phone number
\Email\\ % Your email address
\URL % Your URL
}
\hspace{-1.3in} % Horizontal position of this block, increase to move left, decrease to move right
\vspace{-1in} % Move the letter content up for a more compact look
}

%----------------------------------------------------------------------------------------
%	TO ADDRESS STRUCTURE
%----------------------------------------------------------------------------------------
\def\opening#1{\thispagestyle{empty}
%%% from address
{\centering\fromaddress \vspace{1.1in} \\
%%% Date
\today \hspace*{\fill}\par} % remove \today to not display it
%%% Title of letter
{\raggedright \textbf{\large{Statement on Envisioned Educational Leadership}}}\\[4pt]
{\raggedright \toname \\ \toaddress \par} % Print the to name and address
\vspace{0.15in} % White space after the to address
\noindent #1 % Print the opening line
}

%----------------------------------------------------------------------------------------
%	SIGNATURE
%----------------------------------------------------------------------------------------

\signature{\Who \What}

\long\def\closing#1{
\vspace{0.1in} % Some whitespace after the letter content and before the signature
\noindent % Stop paragraph indentation
\hspace*{0.9cm} % Move the signature right
\parbox{\indentedwidth}{\raggedright
#1 % Print the signature text
\vskip 0.35in % Whitespace between the signature text and your name
%\fromsig}}
}}
%----------------------------------------------------------------------------------------

\begin{document}

%----------------------------------------------------------------------------------------
%	TO ADDRESS
%----------------------------------------------------------------------------------------

\begin{letter}
{Application for Assistant Professor of Teaching in Biometrics and Computation\\
Department of Forest Resources Management\\
Faculty of Forestry\\
University of British Columbia, Vancouver\\
2424 Main Mall\\
Vancouver, BC Canada V6T 1Z4\\
\vspace{0.1in}}

%----------------------------------------------------------------------------------------
%	LETTER CONTENT
%----------------------------------------------------------------------------------------

\opening{The following are a few of my core beliefs related to leadership in education.}\\

\noindent
\textbf{\emph{Promote full, passionate engagement.}} When we--students, staff, faculty and administrators--maintain our comfort zones and meet our minimum requirements, we preserve the educational status quo and prevent ripples in the pond. By contrast, when we proactively seek to set new precedents through inclusivity, creativity, agility and diligence, we transcend perceived boundaries and discover new aptitudes and paradigms. A community whose norms are predicated upon the full and passionate engagement of all its members strengthens its individual members, itself and  adjacent communities. In my view, the limitations of that community's potential for evolution and growth are nonexistent. The promotion of full and passionate engagement should appear in the Jump Start freshman orientation program and should be reinforced throughout the undergraduate experience, in the classroom, in the field, and in student groups alike. It should pervade staff, faculty and administrative milieus. I believe top-down and bottom-up community praise for passionate engagement, communicated persistently by messages both soft and loud, makes striving for excellence cool.  \\

\noindent
\textbf{\emph{Inspire tomorrow's leaders through inspired leadership.}}  Much like the practice of sustainable forest management itself, a natural byproduct of effective educational leadership is the facilitation of successive cohorts of leaders in practice and education. When I think back on my experiences as a student, I see that much of my leadership initiative in student groups and government, in field lab groups, and in my personal life originated in part from the empowerment I experienced by observing a particularly inspired professor, TA, employer or primary school teacher. The aura that emanates from an educator who teaches and leads with inspiration activates innate leadership qualities in the people in their orbit, and can radiate throughout the entire careers of their students and colleagues. Finding ways to infuse the ordinary with inspiration can require time, effort, and coordination, but the trophic cascade of community benefits always outweighs the costs by a wide margin. I believe great leaders in education lead by doing in an energetic and inspired manner. \\

\noindent
\textbf{\emph{Perpetuate programmatic excellence by embracing both convention and innovation.}} The Faculty of Forestry at UBC established its leadership position among forestry schools through commitment to methods of instruction that stood the test of time, and adoption of new and developing approaches as they emerged. The tension between tradition and new ideas seems omnipresent in a discipline with a history as extensive as the history of forestry. I think this is an overtly positive tension that promotes introspection, growth and advancement. Should we teach to students to use freely-available, open-source geospatial data management software, or the proprietary alternatives used by most employers? To what extent should introductory statistics material be delivered to forestry students by Department of Statistics courses, Department of Forest Resources Management courses in forest biometrics, or both? How might interaction with representatives from industry, provincial and federal agencies, non-governmental organizations, and Forest Professional Regulators of Canada strengthen the undergraduate educational experience, enhance student awareness of potential career trajectories, and boost graduate job placement? \\

\noindent
These are a few examples of questions I would welcome the opportunity to consider with colleagues from a position of educational leadership in the Department of Forest Resources Management at UBC in Vancouver.

\closing{Sincerely,}
%----------------------------------------------------------------------------------------

\includegraphics[width=.3\textwidth]{transp_signature_1.png}

\textbf{George C. Gaines III}

\end{letter}

\end{document}

%% end of file `template.tex'.